# Editoria  

This is the Editoria monorepo.  

It consists of the main Editoria application, as well as the different [Pubsweet](https://gitlab.coko.foundation/pubsweet) components and helper libraries that the app is made of.  

This application is being developed by the [Coko Foundation](https://coko.foundation/) for the Editoria Community.  
For more information, visit the project's [website](https://editoria.pub/) or our [chat channel](https://mattermost.coko.foundation/coko/channels/editoria).  
For the editor that Editoria uses, see its related project [Wax](https://gitlab.coko.foundation/wax/wax).  

## Community Roadmap # 2 (DONE)
The current features on our list are the following:

| PROJECT       | DESCRIPTION                                                               | ISSUE # | IN PROGRESS | DONE |
|---------------|---------------------------------------------------------------------------|---------|-------------|------|
| EDITORIA      | Export Overhaul                                                           | #254 #307 #318 #319 #320 #321|             |     ✔   |
| WAX           | Wax Upgrades                                                              | #274 #281 #284 #285          |           |      ✔  |
| EDITORIA      | Paged.js enhancements                                                     | #322 #295 #261 #257 #256 |         |   ✔     |
| EDITORIA      | "Include in TOC" feature                                                  | #323    |                                  |   ✔     |
| EDITORIA      | "Save" button in Paged.js integration                                     | #288    |                                  |     ✔   |
| XSWEET        | Add Autonumbering to part components                                      | #260    |                                  |  ✔      |
| EDITORIA      | Replace INK with PubSweet Job Manager                                     | #255    |                                 |     ✔   |
| EDITORIA      | Add profile page per user                                                 | #293    |                              |    ✔    |
| EDITORIA      | Simpler navigation to/from components from within Wax                     | #276    |                                  |    ✔    |

## Editoria's flavors
Currently the application exists in three different flavours each one containing its' own customizations based on required functionalities  

* [Editoria Vanilla](https://gitlab.coko.foundation/editoria/editoria-vanilla)  
* [Editoria UCP](https://gitlab.coko.foundation/editoria/ucp)  
* [Editoria BookSprints](https://gitlab.coko.foundation/editoria/booksprints)  

The vanilla flavor includes all the features implemented by the development team and should be considered as the referesh application. 

## I want to try it  

In order to try the application, you should navigate and clone one of its' flavors and follow the getting up and running instructions




