# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.1.2"></a>
## [1.1.2](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-asset-manager@1.1.1...editoria-asset-manager@1.1.2) (2020-07-17)


### Bug Fixes

* **asset manager:** optimise get files query ([8d403de](https://gitlab.coko.foundation/editoria/editoria/commit/8d403de))




<a name="1.1.1"></a>
## [1.1.1](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-asset-manager@1.1.0...editoria-asset-manager@1.1.1) (2020-06-19)




**Note:** Version bump only for package editoria-asset-manager

<a name="1.1.0"></a>
# 1.1.0 (2020-06-19)


### Features

* **platform:** asset manager and file hosting ([81c9d29](https://gitlab.coko.foundation/editoria/editoria/commit/81c9d29))
